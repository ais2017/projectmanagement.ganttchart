import datetime

class GenericError(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class Project:
    def __init__(self, project_uid: str, title: str, start_project: datetime.datetime, end_project: datetime.datetime):
        self.project_uid: str = project_uid
        self.title: str = title
        self.start_project: datetime = start_project
        self.end_project: datetime = end_project
        self.calendar_plans = []
        if self.start_project > self.end_project:
            raise GenericError("Project start date can not be longer than Project end date!")

    def get_project_uid(self) -> str:
        return self.project_uid

    def get_project_title(self) -> str:
        return self.title

    def get_project_start(self) -> datetime:
        return self.start_project

    def get_project_end(self) -> datetime:
        return self.end_project

    def get_project_plans(self) -> list:
        return self.calendar_plans

    def create_calendar_plan(self, plan_title, plan_type, month):
        new_plan = CalendarPlan (plan_title, plan_type, month)
        self.calendar_plans.append(new_plan)

class CalendarPlan:

    def __init__(self, plan_title: str, plan_type: str, month: str):
        self.plan_title: str = plan_title
        self.plan_type: str = plan_type
        self.month: str = month
        self.tasks: list = []
        if self.plan_type is None:
            raise GenericError("Incorrect plan type!")
        if self.plan_title is None:
            raise GenericError("Incorrect plan title!")

    def get_calendar_plan_title(self) -> str:
        return self.plan_title

    def get_calendar_plan_type(self) -> str:
        return self.plan_type

    def get_calendar_plan_month(self) -> str:
        return self.month

    def get_calendar_plan_tasks(self) -> list:
        return self.tasks

    def create_task(self, title, task_uid, start_date, end_date, progress):
        new_task = Task(title, task_uid, start_date, end_date, progress)
        self.tasks.append(new_task)

class Task:
    pass

class Task:
    def __init__(self, title: str, task_uid: str, start_date: datetime.datetime, end_date: datetime.datetime,
                         progress: int):
        self.title: str = title
        self.task_uid: str = task_uid
        self.tasks_relationships_end_start: list = []
        self.tasks_relationships_start_start: list = []
        self.tasks_relationships_end_end: list = []
        self.tasks_relationships_start_end: list = []
        self.start_date: datetime = start_date
        self.end_date: datetime = end_date
        self.progress: int = progress
        self.children_task: list = []
        self.parent_task: list = []
        if self.progress > 100 or self.progress < 0:
            raise GenericError("Incorrect task progress!")
        if self.start_date > self.end_date:
            raise GenericError("Incorrect task date!")
        if self.title is None:
            raise GenericError("Incorrect task title!")

    def get_task(self) -> str:
        return self.task_uid

    def get_task_title(self) -> str:
        return self.title

    def get_task_start_date(self) -> datetime.datetime:
        return self.start_date

    def get_task_end_date(self) -> datetime.datetime:
        return self.end_date

    def get_task_progress(self) -> int:
        return self.progress

    def set_start_date(self, start_date: datetime.datetime):
        self.start_date: datetime = start_date
        if self.start_date > self.end_date:
            raise GenericError("Task start date can not be longer than Task end date!")

    def set_end_date(self, end_date: datetime.datetime):
        self.end_date: datetime = end_date
        if self.end_date < self.start_date:
            raise GenericError("Task end date cannot be less than Task start date!")

    def set_progress(self, progress: int):
        self.progress: int = progress
        if progress < 0 or progress > 100:
            raise GenericError("Incorrect task progress!")

            # add_set_get_relationships ES

    def add_relationships_end_start(self, task_uid):
        self.tasks_relationships_end_start.append(task_uid)

    def remove_relationships_end_start(self, task_uid):
        self.tasks_relationships_end_start.remove(task_uid)

    def get_relationships_end_start(self) -> list:
        return self.tasks_relationships_end_start

            # add_set_get_relationships SS

    def add_relationships_start_start(self, task_uid):
        self.tasks_relationships_start_start.append(task_uid)

    def remove_relationships_start_start(self, task_uid):
        self.tasks_relationships_start_start.remove(task_uid)

    def get_relationships_start_start(self) -> list:
        return self.tasks_relationships_start_start

            # add_set_get_relationships EE

    def add_relationships_end_end(self, task_uid):
        self.tasks_relationships_end_end.append(task_uid)

    def remove_relationships_end_end(self, task_uid):
        self.tasks_relationships_end_end.remove(task_uid)

    def get_relationships_end_end(self) -> list:
        return self.tasks_relationships_end_end

            # add_set_get_relationships SE

    def add_relationships_start_end(self, task_uid):
        self.tasks_relationships_start_end.append(task_uid)

    def get_relationships_start_end(self) -> list:
        return self.tasks_relationships_start_end

    def remove_relationships_start_end(self, task_uid):
        self.tasks_relationships_start_end.remove(task_uid)

            # add_remove_child_parent

    def add_child(self, task: Task):
        self.children_task.append(task)

    def get_child(self) -> list:
        return self.children_task

    def remove_child(self, task: Task):
        self.children_task.remove(task)

    def add_parent(self, task: Task):
        self.parent_task.append(task)

    def get_parent(self) -> list:
        return self.parent_task

    def remove_parent(self):
        self.parent_task.clear()

import datetime
import unittest


from app.init import Project, CalendarPlan, Task, GenericError

class ModelTest(unittest.TestCase):

    def setUp(self):

        self.project = Project(
            project_uid="123",
            title="Test",
            start_project=datetime.datetime(2018, 9, 11),
            end_project=datetime.datetime(2018, 11, 15),
        )

        self.plan1 = CalendarPlan(
            plan_title="NewPlan",
            plan_type="Actual",
            month="November"
        )

        self.plan2 = CalendarPlan(
            plan_title="GeneralPlan",
            plan_type="Basic",
            month="September"
        )

        self.task1 = Task(
            title="OldTask",
            task_uid="10",
            start_date=datetime.datetime(2018, 9, 11),
            end_date=datetime.datetime(2018, 11, 1),
            progress=100
        )

        self.task2 = Task(
            title="NewTask",
            task_uid="11",
            start_date=datetime.datetime(2018, 11, 1),
            end_date=datetime.datetime(2018, 11, 10),
            progress=30
        )

        self.task3 = Task(
            title="Task3",
            task_uid="12",
            start_date=datetime.datetime(2018, 11, 1),
            end_date=datetime.datetime(2018, 11, 10),
            progress=30
        )

        self.task4 = Task(
            title="Task4",
            task_uid="13",
            start_date=datetime.datetime(2018, 11, 10),
            end_date=datetime.datetime(2018, 11, 15),
            progress=0
        )


    def tearDown(self):
        self.project = None
        self.plan1 = None
        self.plan2 = None
        self.task1 = None
        self.task2 = None
        self.task3 = None
        self.task4 = None

    #tests tasks

    def test_task_right(self):
        self.assertIsInstance(Task(title="Task6", task_uid="12", start_date=datetime.datetime(2018, 11, 1),
                                   end_date=datetime.datetime(2018, 11, 10), progress=10), Task)

    def test_task_wrong_1(self):
        self.assertRaises(GenericError, Task, title="Task6", task_uid="14",
            start_date=datetime.datetime(2018, 11, 1), end_date=datetime.datetime(2018, 11, 10), progress=101)

    def test_task_wrong_2(self):
        self.assertRaises(GenericError, Task, title="Task6", task_uid="14",
            start_date=datetime.datetime(2018, 11, 11), end_date=datetime.datetime(2018, 11, 10), progress=60)

    def test_task_wrong_3(self):
        self.assertRaises(GenericError, Task, title=None, task_uid="14",
            start_date=datetime.datetime(2018, 11, 1), end_date=datetime.datetime(2018, 11, 10), progress=70)


    def test_get_task(self):
        self.assertEqual(self.task1.get_task(), "10")

    def test_get_title(self):
        self.assertEqual(self.task1.get_task_title(), "OldTask")

    def test_get_task_start_date(self):
        self.assertEqual(self.task1.get_task_start_date(), datetime.datetime(2018, 9, 11))

    def test_get_task_end_date(self):
        self.assertEqual(self.task1.get_task_end_date(), datetime.datetime(2018, 11, 1))

    def test_get_task_progress(self):
        self.assertEqual(self.task1.get_task_progress(), 100)

    def test_set_start_date_right(self):
        self.task1.set_start_date(datetime.datetime(2018, 10, 28))
        self.assertEqual(self.task1.get_task_start_date(), datetime.datetime(2018, 10, 28))

    def test_set_start_date_wrong(self):
        with self.assertRaises(GenericError):
            #print(self.task1.get_task_start_date())
            #print(self.task1.get_task_end_date())
            self.task1.set_start_date(datetime.datetime(2018, 11, 28))

    def test_set_end_date_right(self):
        self.task1.set_end_date(datetime.datetime(2018, 11, 11))
        self.assertEqual(self.task1.get_task_end_date(), datetime.datetime(2018, 11, 11))

    def test_set_end_date_wrong(self):
        with self.assertRaises(GenericError):
            #print(self.task1.get_task_start_date())
            #print(self.task1.get_task_end_date())
            self.task1.set_end_date(datetime.datetime(2018, 9, 1))

    def test_set_progress_right(self):
        self.task1.set_progress(70)
        self.assertEqual(self.task1.get_task_progress(), 70)

    def test_set_progress_wrong(self):
        with self.assertRaises(GenericError):
            self.task1.set_progress(111)

    # tests set relationships ES

    def test_set_relationships_end_start(self):
        self.task1.add_relationships_end_start("11")
        self.task1.remove_relationships_end_start("11")
        out_task: Task = None
        for i, task in enumerate(self.task1.get_relationships_end_start()):
            if task == "11":
                out_task = task
                break
        self.assertIsNone(out_task)
        self.task1.add_relationships_end_start("11")
        for i, task in enumerate(self.task1.get_relationships_end_start()):
            if task == "11":
                out_task = task
                break
        self.assertIsInstance(out_task, str)

    # tests set relationships SS

    def test_set_relationships_start_start(self):
        self.task2.add_relationships_start_start("12")
        self.task2.remove_relationships_start_start("12")
        out_task: Task = None
        for i, task in enumerate(self.task2.get_relationships_start_start()):
            if task == "12":
                out_task = task
                break
        self.assertIsNone(out_task)
        self.task2.add_relationships_start_start("12")
        for i, task in enumerate(self.task2.get_relationships_start_start()):
            if task == "12":
                out_task = task
                break
        self.assertIsInstance(out_task, str)

    # tests set relationships EE

    def test_set_relationships_end_end(self):
        self.task3.add_relationships_end_end("11")
        self.task3.remove_relationships_end_end("11")
        out_task: Task = None
        for i, task in enumerate(self.task3.get_relationships_end_end()):
            if task == "11":
                out_task = task
                break
        self.assertIsNone(out_task)
        self.task3.add_relationships_end_end("11")
        for i, task in enumerate(self.task3.get_relationships_end_end()):
            if task == "11":
                out_task = task
                break
        self.assertIsInstance(out_task, str)

    # tests set relationships SE

    def test_set_relationships_start_end(self):
        self.task4.add_relationships_start_end("12")
        self.task4.remove_relationships_start_end("12")
        out_task: Task = None
        for i, task in enumerate(self.task4.get_relationships_start_end()):
            if task == "12":
                out_task = task
                break
        self.assertIsNone(out_task)
        self.task4.add_relationships_start_end("12")
        for i, task in enumerate(self.task4.get_relationships_start_end()):
            if task == "12":
                out_task = task
                break
        self.assertIsInstance(out_task, str)

    # test add_remove_child_parent

    def test_add_and_remove_child(self):
        self.task1.add_child(self.task2)
        self.task1.remove_child(self.task2)
        out_task: Task = None
        for i, task in enumerate(self.task1.get_child()):
            if task.get_task() == "11":
                out_task = task
                break
        self.assertIsNone(out_task)
        self.task1.add_child(self.task2)
        for i, task in enumerate(self.task1.get_child()):
            if task.get_task() == "11":
                out_task = task
                break
        self.assertIsInstance(out_task, Task)


    def test_add_and_remove_parent(self):
        self.task2.add_parent(Task(
            title="OldTask",
            task_uid="10",
            start_date=datetime.datetime(2018, 9, 11),
            end_date=datetime.datetime(2018, 11, 1),
            progress=100))
        self.task2.remove_parent()
        out_task: Task = None
        for i, task in enumerate(self.task2.get_parent()):
            if task.get_task() == "10":
                out_task = task
                break
        self.assertIsNone(out_task)
        self.task2.add_parent(Task(
            title="OldTask",
            task_uid="10",
            start_date=datetime.datetime(2018, 9, 11),
            end_date=datetime.datetime(2018, 11, 1),
            progress=100))
        for i, task in enumerate(self.task2.get_parent()):
            if task.get_task() == "10":
                out_task = task
                break
        self.assertIsInstance(out_task, Task)

    #tests calendar plans

    def test_calendar_plan_right(self):
        self.assertIsInstance(CalendarPlan(plan_title="NewPlan", plan_type= "Actual", month="November"),
                              CalendarPlan)

    def test_calendar_plan_wrong_1(self):
        self.assertRaises(GenericError, CalendarPlan, plan_title="NewPlan", plan_type=None, month="November")

    def test_calendar_plan_wrong_2(self):
        self.assertRaises(GenericError, CalendarPlan, plan_title=None, plan_type="Actual", month="November")


    def test_calendar_plan_title(self):
        self.assertEqual(self.plan1.get_calendar_plan_title(), "NewPlan")

    def test_get_calendar_plan_type(self):
        self.assertEqual(self.plan1.get_calendar_plan_type(), "Actual")

    def test_get_calendar_plans_month(self):
        self.assertEqual(self.plan1.get_calendar_plan_month(), "November")


    def test_create_task(self):
        self.plan1.create_task("task20", "20", datetime.datetime(2018, 1, 1), datetime.datetime(2018, 1, 10), 60)
        out_task: Task = None
        for i, task in enumerate(self.plan1.get_calendar_plan_tasks()):
            if task.get_task() == "20":
                out_task = task
                break
        self.assertIsInstance(out_task, Task)

    #tests project

    def test_project_right(self):
        self.assertIsInstance(Project(project_uid="123", title="Test",start_project=datetime.datetime(2018, 9, 11),
                                      end_project=datetime.datetime(2018, 11, 15)), Project)

    def test_project_wrong(self):
        self.assertRaises(GenericError, Project, project_uid="123", title="Test",
                          start_project=datetime.datetime(2018, 12, 11), end_project=datetime.datetime(2018, 11, 15))


    def test_get_project_uid(self):
        self.assertEqual(self.project.get_project_uid(), "123")

    def test_get_project_title(self):
        self.assertEqual(self.project.get_project_title(), "Test")

    def test_get_project_start(self):
        self.assertEqual(self.project.get_project_start(), datetime.datetime(2018, 9, 11))

    def test_get_project_end(self):
        self.assertEqual(self.project.get_project_end(), datetime.datetime(2018, 11, 15))

    def test_create_plan(self):
        self.project.create_calendar_plan("NewPlan", "Elder", "May")
        out_task: CalendarPlan = None
        for i, plan in enumerate(self.project.get_project_plans()):
            if plan.get_calendar_plan_title() == "NewPlan":
                out_task = plan
                break
        self.assertIsInstance(out_task, CalendarPlan)


if __name__ == '__main__':
    unittest.main()